import Foundation
//DATE - 24 JAN 2022
//NAME - ATHARVA PRAMOD KULKARNI
//SWIFT BASICS

//Declare a constant
let a = 5;
//Error -> Cannot assign a new value to a constant
// a = 10
//DECLARE A VARIABLE
var b = 10;
//Variable can be assigned diff values many no of times
b = 20
b = 30;


//TYPE ANNOTATIONS
//IT STATES THAT THE VARIABLE WELCOMEMSG CAN ONLY STORE THE STRING VALUES
//IF YOU TRY TO STORE SOME OTHER VALUES THROWS AN ERROR
var welcomeMsg: String
var anyNumber: Int 

//ERROR 
// welcomeMsg = 200

//WORKS PERFECTLY
welcomeMsg = "Hello from Swift"


//ERROR -> ANY NUMBER HAS ITS TYPE ANNOTATION AS AN INT BUT ASSIGNED A STRING VALUE SO ERROR
// anyNumber = "I am a string"

//WORKS FINE
anyNumber = 20000
print(welcomeMsg)
print(anyNumber)
// print("Hello World")

//TYPE ANNOTATION FOR CONSTANT
let constant1 : String
let constant2 : Int 

//INITIALIZE A CONSTANT
constant1 = "Hii I am a constant"
//ERROR -> CONSTANT CANNOT BE INITIALIZED MORE THAN ONCE
// constant1 = "I am gonna throw an error"

constant2 = 5
//ERROR -> NOT ALLOWED CONSTANT NO REINITIALIZATION
// constant2 = 100
// print(constant2)



//PRINT FUNCTION - > AUTOMATICALLY ADDS A LINE BREAK AFTER PRINTING THE VALUE
// SYNTAX -> print(_:separator:terminator:)
// print(constant1)
// print("I am on another line")

//BY DEFAULT WE HAVE "\n" as the terminator(a line break)
//IF WE PUT THE TERMINATOR OF OUR CHOICE WE CAN REPLACE THE LINE BREAK WITH IT
// AND HENCE CAN CONTROL THE TERMINATOR 
// print(constant2, terminator :" ")
// print("I will be printed on the same  line bec of added terminator above")

//WE CAN ADD ANY CHARACTER AS A TERMINATOR
//IT SHOULD BE A STRING
// print(constant2, terminator :":")
// print("ADDED : AS A TERMINATOR ")



//COMMENTS IN SWIFT 
// texts that are ignored by swift compiler , They are used for the purpose of 
// redability of code

//1)SINGLE LINE COMMENT : // I AM A SINGLE LINE COMMENT
//2)MULTILINE COMMENT : 
 /*  
    I AM A MULTILINE COMMENT
*/


//YOU CAN WRITE NESTED MULTILINE COMMENTS INSIDE A MULTILINE COMMENT
/*
    /*
        THIS IS A NESTED MULTILINE COMMENT 
    */
*/

//SEMICOLONS -> 
//IT IS NOT COMPULSORY TO USE SEMICOLONS FOR A SINGLE STATEMENT
// let a = 5
//BUT IF U WANT TO WRITE MULT STATEMENTS IN A SAME LINE USE SEMICOLONS
// let a = 5;let b = 10;print(a,b);



// INTEGERS
// 1)SIGNED -> +VE,-VE,0
// 2)UNSIGNED -> +VE,0


// min and max
// YOU CAN ACCESS min and max value of a int data type using these
// FOR 8 BIT MIN AND MAX VALUES ARE 0 - 255
// let minValue = UInt8.min LIKE INT_MAX IN CPP
// let maxValue = UInt8.max LIKE INT_MIN IN CPP

//FOR UNSIGNED INTEGERS SWIFT PROVIDES
// UInt


//FLOATING POINT NUMBERS IN SWIFT
// 1)FLOAT -> represents 32 bit fptno 
// 2)DOUBLE -> represents 64bit floating pt no


//TYPE SAFETY/INFERENCE

var x = 1
//BASICALLY WHEN U HAVE INITIALIZAED A VAR USING A INTEGER NUMBER
// SWIFT INFERENCES THAT X SHOULD BE OF TYPE INT 
//SO U AREN'T ALLOWED TO PUT ANY OTHER VALUES THAN INT INTO X;
//ERROR -> 
// x = 5.4


// let q = 25.666
//TYPE INFERENCE -> SWIFT BY DEFAULT TREATES IT AS A DOUBLE
// var val = 5 + 0.56 -> TREATED AS A DOUBLE
//TYPE INFERENCE 
// print(q,val); 













